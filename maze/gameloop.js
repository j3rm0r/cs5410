
let previousTime = performance.now();
var canvas = document.getElementById("mainCanvas");
var context = canvas.getContext("2d");

function processInput(elapsedTime) {
}

function update(elapsedTime) {
}

function render(elapsedTime) {
	drawMaze(10, 10);
}

function gameLoop() {
	let currentTime = performance.now();
	let elapsedTime = currentTime - previousTime;
	previousTime = currentTime;

	processInput(elapsedTime);
	update(elapsedTime);
	render(elapsedTime);

	requestAnimationFrame(gameLoop);
}

gameLoop();

function generateMaze(){

}
function drawMaze(increment, degree) {
	for (i=0; i <= canvas.width/2; i+=increment*30 ){
		context.beginPath();
		context.arc(canvas.width/2,canvas.height/2,i, 0, 2*Math.PI);
		//Draw a line then rotate it
		context.translate(canvas.width/2, canvas.height/2);
		context.moveTo(0, (canvas.height/2)*(-1));
		context.lineTo(0, canvas.height/2);
		context.stroke();
		context.rotate( Math.PI/(degree));
		context.translate((canvas.width/2) * (-1), (canvas.height/2) * (-1));
	}
}