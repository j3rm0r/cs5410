let inputBuffer = {};
let canvas = null;
let context = null;

let imgFloor = new Image();
imgFloor.isReady = false;
imgFloor.onload = function() {
    this.isReady = true;
};
imgFloor.src = 'floor.png';

let size = 5;

let graph = [];
for (let row = 0; row < size; row++) {
    graph.push([]);
    for (let col = 0; col < size; col++) {
        graph[row].push({
            x: col, y: row, edges: {
                n: null,
                s: null,
                w: null,
                e: null
            }
        });
    }
}

let frontier, processed = [];

let maze = prims(graph);

function frontNeighbors(row, column) {
	let neighbors = getNeighbors(row, column);
	let front = [];
	for(let i = 0; i < neighbors.length; i++) {
		let x = neighbors[i].x;
		let y = neighbors[i].y;
		if(frontier.indexOf(graph[y][x]) == -1 && processed.indexOf(graph[y][x]) == -1) {
			front.push(graph[y][x]);
		}
	}
	return front;
}

function getNeighborsInMaze(row, column) {
	let neighborsInMaze = getNeighbors(row, column);
	let neighbors = []
	for(let i = 0; i < neighborsInMaze.length; i++) {
		let x = neighborsInMaze[i].x;
		let y = neighborsInMaze[i].y;
		if(processed.indexOf(graph[y][x]) != -1 ) {
			neighbors.push(graph[y][x]);
		}
	}
	return neighbors;
}

function getNeighbors(row, column) {
	let neighbors = [];
	if(row +1 < size ){
		neighbors.push(graph[row +1][column]);
	}
	if(row -1 >= 0 ){
			neighbors.push(graph[row -1][column]);
	}
	if(column +1 < size ){
		neighbors.push(graph[row][column +1]);
	}
	if(column -1 >= 0 ){
			neighbors.push(graph[row][column -1]);
	}
	return neighbors
}

function prims(graph) {
	//randomly select a cell in the graph, add it to the maze and make all it's neighbors a member of the frontier
	let row = Math.floor(Math.random() * size);
	let column = Math.floor(Math.random() * size);
	//console.log("Selected " + column + " " + row );

	let chosenMazeCell = graph[row][column];
	processed = [];	
	frontier = [];
	processed.push(graph[row][column]);
// make frontier grow before
	frontier = frontier.concat(frontNeighbors(row, column));

	while (frontier.length) {
	//randomly choose a cell in the frontier, choose one of it's neighbor cells that is in the maize, remove the wall between them and add that cell to the maize
		let index = Math.floor(Math.random() * frontier.length);
		chosenFrontierCell = frontier[index];
		
		x = chosenFrontierCell.x;
		y = chosenFrontierCell.y;
	//	console.log("the frontier cell " + x + " " + y + " was chosen");
		
		
		// get a cell that is in the maize but borders the frontier cell
		let neighbors = getNeighborsInMaze(y,x);
		let index2 = Math.floor(Math.random() * neighbors.length);
		let neighbor = neighbors[index2];

		if(neighbors.length == 0) {	
			frontier.splice(index,1);
			continue;
		} else {
			let col = neighbor.x;
			let row = neighbor.y;
			console.log("Cell " + x + " " + y + " joined to " + col + " " + row);
			
			if(x === col) {
				if(y > row) {
					graph[row][x].edges.s = graph[y][x];
					graph[y][x].edges.n = graph[row][x];
				}else if (y < row) {
					graph[row][x].edges.n = graph[y][x];
					graph[y][x].edges.s = graph[row][x];
				
				}else {
					console.log("x === row and y == col");	
				}
				
			} else if (y === row) {
				if(x > col) {
					graph[y][col].edges.e = graph[y][x];
					graph[y][x].edges.w = graph[y][col];
						
				}else if (x < col) {
					graph[y][col].edges.w = graph[y][x];
					graph[y][x].edges.e = graph[y][col];

				}else {
					console.log("y === col and x === row");
				}
			}
			
			processed.push(graph[y][x]);
			frontier = frontier.concat(frontNeighbors(y, x));
			frontier.splice(index,1);
			console.log("Cell " + x + " " + y + " has been removed from the frontier");
			
		}	
	}
	//maze[row][column]
	
	return graph;
}


function drawCell(cell) {

    if (imgFloor.isReady) {
        context.drawImage(imgFloor,
        cell.x * (1000 / size), cell.y * (1000 / size),
        1000 / size, 1000 / size);
    }
			//www.w3schools.com/jsref/jsref_concat_array.aspet modified = false;	

    if (cell.edges.n === null) {
        context.moveTo(cell.x * (1000 / size), cell.y * (1000 / size));
        context.lineTo((cell.x + 1) * (1000 / size), cell.y * (1000 / size));
        //context.stroke();
    }

    if (cell.edges.s === null) {
        context.moveTo(cell.x * (1000 / size), (cell.y + 1) * (1000 / size));
        context.lineTo((cell.x + 1) * (1000 / size), (cell.y + 1) * (1000 / size));
        //context.stroke();
    }

    if (cell.edges.e === null) {
        context.moveTo((cell.x + 1) * (1000 / size), cell.y * (1000 / size));
        context.lineTo((cell.x + 1) * (1000 / size), (cell.y + 1) * (1000 / size));
        //context.stroke();
    }

    if (cell.edges.w === null) {
        context.moveTo(cell.x * (1000 / size), cell.y * (1000 / size));
        context.lineTo(cell.x * (1000 / size), (cell.y + 1) * (1000 / size));
        //context.stroke();
    }

    //
    // Can do all the moveTo and lineTo commands and then render them all with a single .stroke() call.
    context.stroke();
}

function renderCharacter(character) {
    if (character.image.isReady) {
        context.drawImage(character.image,
        character.location.x * (1000 / size), character.location.y * (1000 / size));
    }
}

function moveCharacter(key, character) {
    if (key === 'ArrowDown') {
        if (character.location.edges.s) {
            character.location = character.location.edges.s;
        }
    }
    if (key == 'ArrowUp') {
        if (character.location.edges.n) {
            character.location = character.location.edges.n;
        }
    }
    if (key == 'ArrowRight') {
        if (character.location.edges.e) {
            character.location = character.location.edges.e;
        }
    }
    if (key == 'ArrowLeft') {
        if (character.location.edges.w) {
            character.location = character.location.edges.w;
        }
    }
}

function renderMaze() {
    context.strokeStyle = 'rgb(255, 255, 255)';
    context.lineWidth = 6;

    for (let row = 0; row < size; row++) {
        for (let col = 0; col < size; col++) {
            drawCell(maze[row][col]);
        }
    }

    context.beginPath();
    context.moveTo(0, 0);
    context.lineTo(999, 0);
    context.lineTo(999, 999);
    context.lineTo(0, 999);
    context.closePath();
    context.strokeStyle = 'rgb(0, 0, 0)';
    context.stroke();
}

//
// Immediately invoked anonymous function
//
let myCharacter = function(imageSource, location) {
    let image = new Image();
    image.isReady = false;
    image.onload = function() {
        this.isReady = true;
    };
    image.src = imageSource;
    return {
        location: location,
        image: image
    };
}('character.png', maze[0][0]);

function render() {
    context.clearRect(0, 0, canvas.width, canvas.height);

    renderMaze();
    renderCharacter(myCharacter);
}

function processInput() {
    for (input in inputBuffer) {
        moveCharacter(inputBuffer[input], myCharacter);
    }
    inputBuffer = {};
}

function gameLoop() {
    processInput();
    render();

    requestAnimationFrame(gameLoop);

}

function initialize() {
    canvas = document.getElementById('canvas-main');
    context = canvas.getContext('2d');

    window.addEventListener('keydown', function(event) {
        inputBuffer[event.key] = event.key;
    });

    requestAnimationFrame(gameLoop);
}
