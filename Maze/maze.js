let inputStage = {};
let numRows = 5;


let previousTime = performance.now();

let imgFloor = new Image();
imgFloor.isReady = false;
imgFloor.onload = function() {
	this.isReady = true;
};

imgFloor.src = 'floor.png';

function createCharacter(imageSource, location) {
	let image = new Image();
	image.isReady = false;
	image.onload = function() {
		this.isReady = true;
	};
	image.src = imageSource;
	return {
		location: location,
		image: image
	};
}

let maze = [];
for (let row = 0; row < numRows; row++) {
	maze.push([]);
	for (let col = 0; col < numRows; col++) {
		maze[row].push({
			x: col, y: row, edges: {
				n: null,
				s: null,
				w: null,
				e: null
            },
            visited:false,
		});
	}
}

function drawCell(cell) {

	if (imgFloor.isReady) {
		context.drawImage(imgFloor,
		cell.x * (canvas.width/ numRows), cell.y * (canvas.width / numRows),
		canvas.width/ numRows, canvas.width/ numRows);
	}

	if (cell.edges.n === null) {
		context.moveTo(cell.x * (canvas.width / numRows), cell.y * (canvas.height / numRows));
		context.lineTo((cell.x + 1) * (canvas.width / numRows), cell.y * (canvas.height / numRows));
	}

	if (cell.edges.s === null) {
		context.moveTo(cell.x * (canvas.width / numRows), (cell.y + 1) * (canvas.height / numRows));
		context.lineTo((cell.x + 1) * (canvas.width / numRows), (cell.y + 1) * (canvas.height / numRows));
	}

	if (cell.edges.e === null) {
		context.moveTo((cell.x + 1) * (canvas.width / numRows), cell.y * (canvas.height / numRows));
		context.lineTo((cell.x + 1) * (canvas.width / numRows), (cell.y + 1) * (canvas.height / numRows));
	}

	if (cell.edges.w === null) {
		context.moveTo(cell.x * (canvas.width / numRows), cell.y * (canvas.height / numRows));
		context.lineTo(cell.x * (canvas.width / numRows), (cell.y + 1) * (canvas.height / numRows));
	}
	context.stroke();
}

function moveCharacter(keyCode, character) {
	console.log('keyCode: ', keyCode);
	if (keyCode === 40) {
		if (character.location.edges.s) {
			character.location = character.location.edges.s;
		}
	}
	if (keyCode == 38) {
		if (character.location.edges.n) {
			character.location = character.location.edges.n;
		}
	}
	if (keyCode == 39) {
		if (character.location.edges.e) {
			character.location = character.location.edges.e;
		}
	}
	if (keyCode == 37) {
		if (character.location.edges.w) {
			character.location = character.location.edges.w;
		}
	}
	if (keyCode == 72) {
        //hint
    }
    if (keyCode == 66){
        //breadCrumbs
    }
    if (keyCode ==80) {
        //path
    }
    if (keyCode == 89) {
        //score
    }
}

function renderCharacter(character) {
	if (character.image.isReady) {
		context.drawImage(character.image,
		character.location.x * (canvas.width / numRows), character.location.y * (canvas.height / numRows));
	}
}
function renderMaze() {
	context.strokeStyle = 'rgb(255, 255, 255)';
	context.lineWidth = 6;

	for (let row = 0; row < numRows; row++) {
		for (let col = 0; col < numRows; col++) {
			drawCell(maze[row][col]);
		}
	}

	context.beginPath();
	context.moveTo(0, 0);
	context.lineTo(canvas.height-1, 0);
	context.lineTo(canvas.height-1, canvas.width-1);
	context.lineTo(0, canvas.width-1);
	context.closePath();
	context.strokeStyle = 'rgb(0, 0, 0)';
	context.stroke();
}

function render(elapsedTime) {
	context.clear();

	renderMaze();
	renderCharacter(myCharacter);
}

function processInput(elapsedTime) {
	for (input in inputStage) {
		moveCharacter(inputStage[input], myCharacter);
	}
	inputStage = {};
}
let canvas = null;
let context = null;
var myCharacter = createCharacter('character.png', maze[0][0]);

function update(elapsedTime) {
    //do nothing for now
}
function generateMaze() {
	var notYetComplete = true;
	var drestoryWall= true;
    var count = 25;
	var stack = [];
    //mark the inital cell as visited
    var InitCell=maze[0][0];
	stack.push(InitCell);
    InitCell.visited= true;
	var currentCell = InitCell;
	//figure out what's happening then change this
    while(count>0 && stack.length > 0) {
		//see if the neighbors have been visited
		var neighbors = getNeighbors(currentCell.x, currentCell.y);
		console.log(neighbors.length);
        if(neighbors.length-1 >=0) {
			// console.log("flag1")
			//push the current cell to the stack
			stack.push(maze[currentCell.x][currentCell.y])
			//pick one at random
			var neighbor = neighbors[Math.floor(Math.random() * neighbors.length)];
			// destroy the wall between teh current cell and the chosen cell
			destroyWall(currentCell, neighbor);
			//make the chosen cell the current cell and mark as 
			if(maze[currentCell.x][currentCell.y].visited){}
			else{
				maze[neighbor.x][neighbor.y].visited=true;
				count--;
			}
			currentCell = maze[neighbor.x][neighbor.y];	

        }
        else if (stack.length-1 >=0){
            //pop the top cell off and make it the current cell
			currentCell = stack.pop();
			// console.log();
		}
	}
	console.log(maze);
	console.log(stack);
}
function destroyWall(current, neighbor){
	if(current.x - neighbor.x ==0 && current.y - neighbor.y ==1){
		maze[current.x][current.y].edges.s = maze[neighbor.x][neighbor.y];
		maze[neighbor.x][neighbor.y].edges.n = maze[current.x][current.y]; 
	}
	else if(current.x - neighbor.x ==0 && current.y - neighbor.y ==-1){
		maze[current.x][current.y].edges.n = maze[neighbor.x][neighbor.y];
		maze[neighbor.x][neighbor.y].edges.s = maze[current.x][current.y]; 
	}
	else if(current.x - neighbor.x ==1 && current.y - neighbor.y ==0){
		maze[current.x][current.y].edges.e = maze[neighbor.x][neighbor.y];
		maze[neighbor.x][neighbor.y].edges.w = maze[current.x][current.y]; 

	}
	else if(current.x - neighbor.x ==-1 && current.y - neighbor.y ==0){
		maze[current.x][current.y].edges.w = maze[neighbor.x][neighbor.y];
		maze[neighbor.x][neighbor.y].edges.e = maze[current.x][current.y]; 

	}
}
function getNeighbors(x,y){
	StupidArrayName=[]
    if (x+1 < numRows && !maze[x+1][y].visited){
		StupidArrayName.push(maze[x+1][y]);
	}
    if (x>0 && !maze[x-1][y].visited){
        StupidArrayName.push(maze[x-1][y]);
    }
    if (y+1 < numRows && !maze[x][y+1].visited){
        StupidArrayName.push(maze[x][y+1]);
    }
    if (y > 0 && !maze[x][y-1].visited) {
        StupidArrayName.push(maze[x][y-1]);
	}
	return StupidArrayName;
}

function initialize() {
	canvas = document.getElementById('myCanvas');
	context = canvas.getContext('2d');

    generateMaze();
	CanvasRenderingContext2D.prototype.clear = function() {
		this.save();
		this.setTransform(1, 0, 0, 1, 0, 0);
		this.clearRect(0, 0, canvas.width, canvas.height);
		this.restore();
	};

	window.addEventListener('keydown', function(event) {
		//moveCharacter(event.keyCode, myCharacter);
		inputStage[event.keyCode] = event.keyCode;
	});

	requestAnimationFrame(gameLoop);
}

function gameLoop() {

	let currentTime = performance.now();
	let elapsedTime = currentTime - previousTime;
	previousTime = currentTime;

	processInput(elapsedTime);
	update(elapsedTime);
	render(elapsedTime);

	requestAnimationFrame(gameLoop);
}
