let previousTime = performance.now();

var canvas = document.getElementById("myCanvas");
var ctx = canvas.getContext("2d");

var ballRadius = 10;
var ballColor = "#000000"
var x = canvas.width/2;
var y = canvas.height-30;
var dx = 2;
var dy = -2;
var count = 1;

//gotta have 8 rows of 14 bricks 2 rows of each color yellow, orange, blue and green
var brickRows = 8;
var brickColumns=14;
var brickPadding=5;
var brickOffset= (canvas.width-brickPadding)/brickColumns;
var brickWidth = brickOffset-(brickPadding);
var brickHeight = canvas.height/(4* brickRows);
var brickColors = ["#00ff00","#00ff00","#0000ff" , "#0000ff","#FFA500", "#FFA500","#FFFF00","#FFFF00" ]
var brickColor = brickColors[0];

var bricks=[];
for(i=0; i<brickRows;i++) {
    bricks[i] = [];
    for(j=0; j<brickColumns;j++){
        bricks[i][j]= {x: 0, y:0, status:0, frontline:0}
    }
}
//Add the first row to the frontline

for(i=0; i<brickColumns;i++) {
    bricks[brickRows-1][i].frontline=1;
}

var paddleHeight = 10;
var paddleWidth = 75;
var paddleX = (canvas.width-paddleWidth)/2;
var paddleCenter = paddleX-(paddleWidth/2);

var rightPressed = false;
var leftPressed = false;

document.addEventListener("keydown", keyDownHandler, false);
document.addEventListener("keyup", keyUpHandler, false);

function keyDownHandler(e) {
    if(e.keyCode == 39) {
        rightPressed = true;
    }
    else if(e.keyCode == 37) {
        leftPressed = true;
    }
}

function keyUpHandler(e) {
    if(e.keyCode == 39) {
        rightPressed = false;
    }
    else if(e.keyCode == 37) {
        leftPressed = false;
    }
}

function drawPaddle() {
    ctx.beginPath();
    ctx.rect(paddleX, canvas.height-paddleHeight, paddleWidth, paddleHeight);
    ctx.fillStyle = "#0095DD";
    ctx.fill();
    ctx.closePath();
}

function drawBall() {
    ctx.beginPath();
    ctx.arc(x, y, ballRadius, 0, Math.PI*2);
    ctx.fillStyle = ballColor;
    ctx.fill();
    ctx.closePath();
}

function drawBricks(){
    for(i=0; i<brickRows;i++) {
        brickColor = brickColors[i];
        for(j=0; j<brickColumns;j++){
            var xBrick=brickPadding + (j*brickOffset); 
            var yBrick=brickPadding + (i*((brickHeight)+brickPadding)); 
            bricks[i][j].x = xBrick;
            bricks[i][j].y = yBrick;
            if (bricks[i][j].status == 0 && bricks[i][j].frontline == 1){
                ctx.beginPath();
                ctx.rect(xBrick, yBrick, brickWidth, brickHeight);
                ctx.fillStyle=brickColor;
                ctx.fill();
                ctx.closePath();
            }
        }
    }
}

function processInput(elapsedTime){

    if(rightPressed && paddleX < canvas.width-paddleWidth) {
        paddleX += 7;
    }
    else if(leftPressed && paddleX > 0) {
        paddleX -= 7;
    }
}

function detectCollision() {
    //Collision with walls
    if(x + dx > canvas.width-ballRadius || x + dx < ballRadius) {
        dx = -dx;
    }
    if(y + dy < ballRadius) {
        dy = -dy;
    }
    //colision with the paddle and the ground
    else if(y + dy > canvas.height-ballRadius-paddleHeight){
        if(x > paddleX && x < paddleX + paddleWidth) {
            //xdirection = (ballcenter - paddleCenter)/ (paddleWidth/2)
            dy = -dy;
            dx = (x-paddleCenter) / (paddleWidth/2);
        }
        else {
            alert("GAME OVER");
            document.location.reload();
        }
    }
    //collision with bricks
    // really you only need to test the bricks that are on the front line
    for(i=0; i<brickRows;i++) {
        for(j=0; j<brickColumns;j++){
            var b = bricks[i][j];
            if (b.frontline = 1) {
                if(x+ballRadius > b.x && x+ballRadius < b.x+brickWidth && y+ballRadius > b.y && y-ballRadius< b.y + brickHeight && b.status == 0){
                    dy = -dy;
                    b.status = 1;
                    //Let's make the brick behind it a member of the frontline
                    bricks[i-1][j].frontline = 1;

                }
            }

        }
    } 

}

function update(elapsedTime) {
    detectCollision();
    x += dx;
    y += dy;
}

function render(elapsedTime) {
    ctx.clearRect(0, 0, canvas.width, canvas.height);
    drawBall();
    drawPaddle();
    drawBricks();
}

function gameLoop() {

	let currentTime = performance.now();
	let elapsedTime = currentTime - previousTime;
	previousTime = currentTime;

    processInput(elapsedTime);
    update(elapsedTime);
    render(elapsedTime);

    requestAnimationFrame(gameLoop);
}

gameLoop();