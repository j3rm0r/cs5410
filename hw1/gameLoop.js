let currentTime = performance.now()
let elapsedTime = performance.now() - currentTime;

let events = [];
let queue = [];
let displayedEvents = [];


class Event {
    constructor(name, interval, count){
        this.name = name;
        this.interval = interval;
		this.intervalLeftovers = interval;
        this.count = count;
		this.remaining = count;
    }
};

function gameLoop(){
	let time = performance.now();
    elapsedTime = time - currentTime;
	currentTime =  time;

    processInput(elapsedTime);
    update(elapsedTime);
    render();

    requestAnimationFrame(gameLoop);
}

function processInput(elapsedTime) {
    // Javascript will let you push something that doesn't exist
    // we don't want it to
    if(queue.length != 0) {
        events.push(queue.pop());
    }    
}

function update(elapsedTime) {
	// if event.interval - elapsed time < 0 trigger the event and decrement event.remaining
	// 
	let i;
	for (i = 0; i < events.length; i++) {
		if (events[i].remaining > 0) {
			//update the time remaining
			let overlap = events[i].intervalLeftovers - elapsedTime;
			if (overlap < 0) {
				events[i].intervalLeftovers = events[i].interval + overlap;
				displayedEvents.push(events[i]);
				events[i].remaining += -1;
			} else {
				events[i].intervalLeftovers = events[i].intervalLeftovers - elapsedTime;
			}
		} else {
			//remove it from events
			// events.splice(i,1);
		}
	}
}

function render() {
    // Now just add the ones that are in the events array
    let i;
    for (i=0; i < displayedEvents.length; i++) {
        let listElement = document.createElement("li");
        let eventMessage = "" + displayedEvents[i].name + " " + displayedEvents[i].remaining;
        let eventText = document.createTextNode(eventMessage);
        listElement.appendChild(eventText);
        let eventList = document.getElementById("eventList");
        eventList.appendChild(listElement);
        eventList.scrollTop = eventList.scrollHeight;
    }
	//empty out the display queue
	displayedEvents=[];
}

function submitForm() 
{
  let formInputs = document.getElementById("newEventForm");
  let newEventName = formInputs.elements[0].value;
		//Parse int turns it into a number
  let newEventInterval = parseInt(formInputs.elements[1].value);
  let newEventCount = parseInt(formInputs.elements[2].value);
  let newEvent = new Event(newEventName, newEventInterval, newEventCount);
  queue.push(newEvent);
}
