
// ------------------------------------------------------------------
myGame.persistence = (function () {
	'use strict';
	
	let highScores = {};
	let controls = {};
	let previousControls= localStorage.getItem('myGame.controls');
	let previousScores = localStorage.getItem('myGame.highScores');

	if (previousScores !== null) {
		highScores = JSON.parse(previousScores);
	}

	if (previousControls !== null) {
		controls = JSON.parse(previousControls);
	}else {
		addControl('Left', 'ArrowLeft');
		addControl('Right', 'ArrowRight');
		addControl('RightRotate', 'd');
		addControl('LeftRotate', 'a');
		addControl('HardDrop', ' ');
		addControl('SoftDrop', 'ArrowDown');
	}

	function addScore(key, value) {
		highScores[key] = value;
		localStorage['myGame.highScores'] = JSON.stringify(highScores);
	}

	function removeScore(key) {
		delete highScores[key];
		localStorage['myGame.highScores'] = JSON.stringify(highScores);
	}

	function resetControls() {
		addControl('Left', 'ArrowLeft');
		addControl('Right', 'ArrowRight');
		addControl('RightRotate', 'd');
		addControl('LeftRotate', 'a');
		addControl('HardDrop', ' ');
		addControl('SoftDrop', 'ArrowDown');
	}

	function addControl(key, value) {
		controls[key] = value;
		localStorage['myGame.controls'] = JSON.stringify(controls);
	}
	
	function removeControl(key) {
		delete controls[key];
		localStorage['myGame.controls'] = JSON.stringify(controls);
	}

	function reportScore() {
		let htmlNode = document.getElementById('highscores-list');
		
		htmlNode.innerHTML = '';
		for (let key in highScores) {
			let score = document.createTextNode('Time: ' + key + ' Score: ' + highScores[key])
			let li_score = document.createElement('li');
			li_score.appendChild(score);
			htmlNode.appendChild(li_score);
		}
		htmlNode.scrollTop = htmlNode.scrollHeight;
	}

	function reportControl() {
		let htmlNode = document.getElementById('controls-list');
		
		htmlNode.innerHTML = '';
		for (let key in controls) {
			let control = document.createTextNode('"'+ controls[key] + '"')
			let li_control= document.createElement('li');
			li_control.appendChild(control);
			htmlNode.appendChild(li_control);
		}
		htmlNode.scrollTop = htmlNode.scrollHeight;
	}

	return {
		addScore : addScore,
		removeScore : removeScore,
		reportScore : reportScore,
		addControl: addControl,
		removeControl: removeControl,
		reportControl: reportControl,
		resetControls: resetControls
	};
}());

