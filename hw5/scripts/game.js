myGame.game = (function(screens) {
	'use strict';
	
	function displayScreen(name) {
		let active = document.getElementsByClassName('active');
		for (let i = 0; i< active.length; i++) {
			active[i].classList.remove('active');
		}
		
		screens[name].run();

		document.getElementById(name).classList.add('active');
	}
	function init() {
		//init each screen including the game
		let screen = null;
		for (screen in screens) {
			screens[screen].init();
		}
		//make the landing page the default
		//displayScreen('gamePlay');
		displayScreen('setup');
	}

	return {
		init : init,
		displayScreen : displayScreen
	};

}(myGame.screens));

