myGame.screens['gamePlay'] = (function(game, systems, objects, renderer, graphics, input, persistence, sound) {
	'use strict';

	let stop_playing = false;
	let currentTime = performance.now();
	let startTime = performance.now();
	let firingDelay = 250;
	let Score = 0;
	let highScores = [];
	let lives = 3;
	
	let myHighScores = persistence;

	let mySounds = sound;

	// keyboard input
	let myKeyboard = input;

//    let particles = systems.ParticleSystem({
//        center: { x: myShip.center.x, y: myShip.center.y},
//        size: { mean: 6, stdev: 3 },
//        speed: { mean: 45, stdev: 15 },
//        lifetime: { mean: 2, stdev: 1}
//    });
//    let rocketRenderer= renderer.ParticleSystem(particles, graphics, 'scripts/render/assets/particles/yellowlight.png');

	function processInput(elapsedTime) {
		myKeyboard.update(elapsedTime);
	}

	function update(elapsedTime) {
		gameOver();
	}
	


	function render() {
		graphics.clear();
		
		graphics.drawText({
			font: '30px Arial',
			fillStyle: '#F8F869',
			strokeStyle: '#F8F869',
			position: {x: 50, y:50},
			rotation: 0,
			text:"Score: "+ Score + " | Time: " + Math.round((performance.now() - startTime) / 1000) + " | Lives: " + lives
		});
		
	}

	function gameLoop(time) {
		let elapsedTime = time - currentTime;
		currentTime = time;
		processInput(elapsedTime);
		update(elapsedTime);
		render();

		if (!stop_playing) {
			requestAnimationFrame(gameLoop);
		}
	}

	function gameOver() {
		myHighScores.addScore(Math.round((performance.now() - startTime) / 1000), Score);
		stop_playing = true;
		game.displayScreen('landing');
		Score = 0;
	}
	
	function init() {
		
		stop_playing = false;
		mySounds.playSound('audio/music');

		myKeyboard.register('Escape', function() {
			stop_playing = true;
			game.displayScreen('landing');
		});

		startTime = performance.now();

	}

	function run() {
		stop_playing = false;
		startTime = performance.now();
		currentTime = performance.now();
		requestAnimationFrame(gameLoop);
	}

	return {
		init : init,
		run : run
	};

}(myGame.game, myGame.systems, myGame.objects, myGame.render, myGame.graphics, myGame.input, myGame.persistence, myGame.sound));
