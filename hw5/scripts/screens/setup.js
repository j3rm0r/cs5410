myGame.screens['setup'] = (function(game, persistence, input) {
	'use strict';
	
	let myKeyboard = input;
	let myStorage = persistence;

	function bind(key) {
		myStorage.addControl(key, myKeyboard.getLastPressed());
		myStorage.reportControl();
	}

	function update() {
		document.getElementById("setup-current-key").innerHTML = '"' + myKeyboard.getLastPressed() + '"';
	}

	function init() {

		myKeyboard.start();
		document.getElementById("controls-Left").addEventListener('click',
			function() {
				bind('Left');
			});
		document.getElementById("controls-Right").addEventListener('click',
			function() { 
				bind('Right');
			});
		document.getElementById("controls-RightRotate").addEventListener('click',
			function() { 
				bind('RightRotate');
			});
		document.getElementById("controls-LeftRotate").addEventListener('click',
			function() { 
				bind('LeftRotate');
			});
		document.getElementById("controls-SoftDrop").addEventListener('click',
			function() { 
				bind('SoftDrop');
			});
		document.getElementById("controls-HardDrop").addEventListener('click',
			function() { 
				bind('HardDrop');
			});
		
		document.getElementById("setup-default").addEventListener('click',
			function() { 
				myStorage.resetControls();
				myStorage.reportControl();
			});
		document.getElementById("setup-back").addEventListener('click',
			function() { 
				window.removeEventListener('keydown', function(){update();});
				game.displayScreen('landing'); 
			});
		document.getElementById("setup-gamePlay").addEventListener('click',
			function() {
				window.removeEventListener('keydown', function(){update();});
				game.displayScreen('gamePlay'); 
			});
		
	}

	function run() {
		myStorage.reportControl();
		window.addEventListener('keydown', function(){update();});
	}

	return {
		init : init,
		run : run,
		myKeyboard: myKeyboard,
		myStorage: myStorage
	};
}(myGame.game, myGame.persistence, myGame.input));
