myGame.screens["highscores"] = (function(game, objects, render, graphics, input, persistence) {
	
	'use strict';

	function init() {
		document.getElementById('highscores-back').addEventListener('click',
			function() {
				game.displayScreen("landing"); 
			});
	}

	function run() {
		myGame.persistence.reportScore();
	}

	return {
		init : init,
		run : run,
	};
}(myGame.game));
