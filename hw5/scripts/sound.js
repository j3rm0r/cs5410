myGame.sound = (function () {
			
	let sounds = {}

	//------------------------------------------------------------------
	//
	// This function performs the one-time initialization.
	// Written by Dean Mathias
	//------------------------------------------------------------------
	function initialize() {
		'use strict';

		function loadSound(source) {
			let sound = new Audio();
			sound.src = source;
			return sound;
		}

		function loadAudio() {
		//	sounds['audio/sound-2'] = loadSound('scripts/render/assets/audio/Pew__004.wav');
		}

		loadAudio();
	}
	
	initialize();

	//------------------------------------------------------------------
	//
	// Plays the specified audio
	// written by Dean Mathias
	//------------------------------------------------------------------
	function playSound(whichSound) {
	//	sounds[whichSound].play();
	}

	return {
		initialize: initialize,
		playSound: playSound
	};

}());

