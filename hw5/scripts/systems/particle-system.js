myGame.systems.ParticleSystem = function (spec) {
	let rotation = Math.PI/2;
    let nextName = 1;
    let particles = {};

    function create() {
        let size = Random.nextGaussian(spec.size.mean, spec.size.stdev);
        let p = {
            center: { x: spec.center.x, y: spec.center.y },
            size: { width: size, height: size },
            direction: Random.nextCircleVector(),
            speed: Random.nextGaussian(spec.speed.mean, spec.speed.stdev), // pixels per second
            rotation: 0,
            lifetime: Random.nextGaussian(spec.lifetime.mean, spec.lifetime.stdev), // seconds
            alive: 0
        };

        return p;
    }
    
	function addParticles() {
        for (let particle = 0; particle < 2; particle++) {
            particles[nextName++] = createDirected(rotation);
        }
	}

	function explosion(center, direction) {
        for (let particle = 0; particle < 40; particle++) {
			let size = Random.nextGaussian(spec.size.mean, spec.size.stdev);
			let p = {
				center: { x: center.x, y: center.y },
				size: { width: size, height: size },
				// Math.PI/12 a +- range for the rotation
				direction: Random.nextCircleVectorDirected(direction, Math.PI/6),
				speed: Random.nextGaussian(spec.speed.mean, spec.speed.stdev), // pixels per second
				rotation: 0,
				lifetime: Random.nextGaussian(spec.lifetime.mean, spec.lifetime.stdev), // seconds
				alive: 0
			};

			particles[nextName++] = p;
        }
	}

    function update(elapsedTime, center, rot) {
        spec.center.x = center.x;
        spec.center.y = center.y;
		rotation = rot + (Math.PI);

		let removeMe = [];

        elapsedTime = elapsedTime / 1000;

        Object.getOwnPropertyNames(particles).forEach(value => {
            let particle = particles[value];

            particle.alive += elapsedTime;
            particle.center.x += (elapsedTime * particle.speed * particle.direction.x);
            particle.center.y += (elapsedTime * particle.speed * particle.direction.y);

            particle.rotation += particle.speed / 500;

            if (particle.alive > particle.lifetime) {
                removeMe.push(value);
            }
        });

        for (let particle = 0; particle < removeMe.length; particle++) {
            delete particles[removeMe[particle]];
        }
    }

    let api = {
        update: update,
		addParticles: addParticles,
		explosion: explosion,
        get particles() { return particles; }
    };

    return api;
};
