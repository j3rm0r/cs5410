myGame.input = (function() {

	let keyboard = { 
		keys: {},
		handlers: {}
	};

	let listening = false;
	//gotta find a better way to do this
	let characterBuffer = 'a';

	function keyDown(e) {
		keyboard.keys[e.key] = e.timeStamp;
		if(listening){
			characterBuffer =  e.key;
		}
	}
	
	function keyUp(e) {
		delete keyboard.keys[e.key];
	}
	
	keyboard.update = function (time) {
		for (key in keyboard.keys) {
			if(keyboard.keys.hasOwnProperty(key)) {
				if (keyboard.handlers[key]) {
					keyboard.handlers[key](time)
				}
			}
		}
	};
	
	keyboard.start = function () {
		listening = true;
	}
	keyboard.getLastPressed = function() {
		return characterBuffer;
	}
	
	keyboard.register = function (key, handler) {
		keyboard.handlers[key] = handler;
	}

	keyboard.unregister = function (key, handler) {
		delete keyboard.handlers[key];
	}

	window.addEventListener('keydown', keyDown);
	window.addEventListener('keyup', keyUp);
	
	return keyboard;

}());
