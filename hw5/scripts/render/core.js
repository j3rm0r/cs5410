myGame.graphics = (function() {
    'use strict';

    let canvas = document.getElementById('canvas');
    let context = canvas.getContext('2d');
    // --------------------------------------------------------------
    // sets the canvas height and widhth to the window height and width
	//
	// written by stackoverlow user devyn
	// https://stackoverflow.com/questions/1664785/resize-html5-canvas-to-fit-window
    // --------------------------------------------------------------
	context.canvas.height = window.innerHeight;
	context.canvas.width = window.innerWidth;

    // --------------------------------------------------------------
    //
    // clears the canvas 
    // Written by Dean Mathias
    // --------------------------------------------------------------

    function clear() {
        context.clearRect(0, 0, canvas.width, canvas.height);
    }

    // --------------------------------------------------------------
    //
    // Draws a texture to the canvas with the following specification:
    //    image: Image
    //    center: {x: , y: }
    //    size: { width: , height: }
	//    rotation: 
    // Written by Dean Mathias
    // --------------------------------------------------------------
    function drawTexture(image, center, rotation, size) {
        context.save();

        context.translate(center.x, center.y);
        context.rotate(rotation);
        context.translate(-center.x, -center.y);

        context.drawImage(
            image,
            center.x - size.width / 2,
            center.y - size.height / 2,
            size.width, size.height);

        context.restore();
    }
    // --------------------------------------------------------------
    //
    // Draws a text to the canvas with the following specification:
	// 		font:
	// 		fillStyle,
	// 		strokeStyle,
	// 		position: {x:, y:},
	// 		rotation: 
    // Written by Dean Mathias
    // --------------------------------------------------------------

    function drawText(spec) {
        context.save();

        context.font = spec.font;
        context.fillStyle = spec.fillStyle;
        context.strokeStyle = spec.strokeStyle;
        context.textBaseline = 'top';

        context.translate(spec.position.x, spec.position.y);
        context.rotate(spec.rotation);
        context.translate(-spec.position.x, -spec.position.y);


        context.fillText(spec.text, spec.position.x, spec.position.y);
        context.strokeText(spec.text, spec.position.x, spec.position.y);

        context.restore();
    }

    let api = {
        get canvas() { return canvas; },
        clear: clear,
        drawTexture: drawTexture,
        drawText: drawText
    };

    return api;
}());
