'use strict';
let present = require('present');

function processInput(elapsedTime) {

}

function update(elapsedTime, currentTime) {

}

function updateClients(elapsedTime) {

}

function gameLoop(currentTime, elapsedTime) {
    processInput(elapsedTime);
    update(elapsedTime, currentTime);
    updateClients(elapsedTime);
}

function newSocketIO(server) {
    //for now let's make it send updates to all the things that connect to it
    let io = require('socket.io')(server);

    io.on('connection', function(socket){
        console.log('New connection', socket.id);
        
        socket.on('disconnect', function() {

        });
    });
}

function start(server){
    newSocketIO(server);
    gameLoop(present(), 0);
}
//This makes start visible to things outside of this space
module.exports.start = start;