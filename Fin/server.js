'use strict';

let http = require('http'),
    path = require('path'),
    fs =  require('fs'),
    game = require('./scripts/server/game');

let mimeTypes = {
    '.js' : 'text/javascript',
    '.html' : 'text/html',
    '.css' : 'text/css',
    '.png' : 'text/png',
    '.jpg' : 'image/jpeg',
    '.mp3' : 'audio/mpeg3'
};
//Server that Dean used for simple game but with funnier messages   
function handleRequest(request, response) {
    let lookup = (request.url === '/')  ? '/index.html' : decodeURI(request.url);
    let file = lookup.substring(1, lookup.length);

    console.log('request: ' + request.url);
    fs.exists(file, function(exists){
        if (exists) {
            console.log ('gonna try to send: ' + lookup);
            fs.readFile(file, function(error, data){
            if (error) {
                response.writeHead(500);
                response.end('The server needs more money');
            } else {
                let headers = {'Content-type': mimeTypes[path.extname(lookup)]};
                response.writeHead(200, headers);
                response.end(data);
            }
            })
        }
        else {
            response.writeHead(404);
            response.end('Oh dear looks like a 404 page');
        }
    })
}

let server = http.createServer(handleRequest);

server.listen(4200, function() {
    game.start(server);
    console.log('server is listening on port 4200 tell it your darkest sins child');
});