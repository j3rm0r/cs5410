let currentTime = performance.now()
let elapsedTime = performance.now() - currentTime;

let delay = 1;//this can be incremented as time goes on
let delayInterval = 100; //this can be decremented as time goes on


let Obstacles = [];
let usedBlocks = [];

let canvas = document.getElementById('id-canvas');
let context = canvas.getContext('2d');

let Score = 0;
let highScores=[];

let Snake = {
	direction : null,
	head: {x:25, y: 25},
	moveRate: 1,
	length: 1,
	leftoverLength: 1,
	blocks: [] 
};

let Food = {
	x: null,
	y: null
};

// those blocks on the border and the ones inside the play area
function setup() {

	Snake.head = {x:25, y: 25};
	Snake.direction = null;
	Snake.blocks = [];
	Snake.length = 1;
	Snake.leftoverLength =1;

	Score = 0;
	
	Obstacles = [];
	usedBlocks = [];

	while (Obstacles.length < 16) {
		let obstacle = {x: (Math.floor(Math.random() * 48) + 1) ,y:( Math.floor(Math.random() * 48) +1)}
		if (Obstacles.indexOf(obstacle) === -1){
			Obstacles.push(obstacle);
		}
	}

	let i;
	for(i=0; i < 50; i++) {
		Obstacles.push({x:i, y:0});
		Obstacles.push({x:0, y:i});
		Obstacles.push({x:49, y:i});
		Obstacles.push({x:i, y:49});
	}
	
	makeFood();
}

function gameOver() {
	highScores.push(Score);
	highScores.sort(function(a,b) {return b-a});
	
	let DOMHighscoreDiv= document.getElementById("Highscores")
	while(DOMHighscoreDiv.hasChildNodes()){
		DOMHighscoreDiv.removeChild(DOMHighscoreDiv.lastChild);
	}
	DOMHighscoreList = document.createElement("ol");
	DOMHighscoreDiv.appendChild(DOMHighscoreList);
	let i;
	for (i=0;i<6 && i < highScores.length;i++) {
		let newEntry = document.createElement("li");
		newEntry.innerText="" + highScores[i];
		DOMHighscoreList.appendChild(newEntry);
	}
	
	setup();
	
}

function makeNewFood() {
	Score = Snake.length;
	Food.x = null;
	Food.y = null;
	makeFood();
}

function makeFood() {
	// don't want the food to take the same place as any of the obstacles
	while (Food.x === null || Food.y === null) {
		let obstacle = {x: (Math.floor(Math.random() * 48) + 1) ,y:( Math.floor(Math.random() * 48) +1)}
		if (Obstacles.indexOf(obstacle) === -1){
			Food = obstacle;
		}
	}
}


function game() {
	let time = performance.now();
	elapsedTime = time - currentTime;
	currentTime =  time;
    update(elapsedTime);
    render();
	requestAnimationFrame(game);
}

function moveSnake() {
	//see if snake died
	//This method of collision detection from https://stackoverflow.com/questions/12462318/find-a-value-in-an-array-of-objects-in-javascript
	if(usedBlocks.find(killer => killer.x === Snake.head.x && killer.y === Snake.head.y)) {
		//Snake got dead
		gameOver();
	}else {

		//see if you ate something 
		if(Snake.head.x === Food.x && Snake.head.y === Food.y) {
			Snake.length +=3;
			makeNewFood();
		}
		// move
		if (Snake.direction === "LEFT" && Snake.head.x + Snake.moveRate <  51 - Snake.moveRate) {
			Snake.head.x += (Snake.moveRate);
		}
		else if (Snake.direction === "RIGHT" && Snake.head.x > 0) {
			Snake.head.x -= Snake.moveRate;
		}
		else if (Snake.direction === "DOWN" && Snake.head.y + Snake.moveRate < 51 - Snake.moveRate) {
			Snake.head.y += (Snake.moveRate);
		}
		else if (Snake.direction === "UP" && Snake.head.y > 0) {
			Snake.head.y -= (Snake.moveRate);
		}
		else if (Snake.direction === null){
			//do nothing
		}

		//try to grow
		let tail = {x: Snake.head.x, y: Snake.head.y} 
		if(Snake.blocks.length < Snake.length) {
			//grow after snake.blocks.length moves
			if(Snake.leftoverLength > 3){
				Snake.blocks.push(tail);
				Snake.blocks.shift();
				Snake.leftoverLength -= 1;
			} else if (Snake.leftoverLength <= 3 && Snake.leftoverLength > 0){
				Snake.blocks.push(tail);
				Snake.leftoverLength -= 1;
			}else if (Snake.leftoverLength <= 0){
				Snake.leftoverLength = Snake.blocks.length;
				Snake.blocks.push(tail);
				Snake.blocks.shift();
			}
		} else {
			Snake.blocks.push(tail);
			Snake.blocks.shift();
		}
	}

	usedBlocks = Obstacles.concat(Snake.blocks);
	usedBlocks.pop();

}

function update(elapsedTime) {
	//give it that nice sweet choppy motion everyone loathes
	if (delay - elapsedTime < 0) {
		moveSnake();
		delay=delayInterval;
	} else {
		delay = (delay-elapsedTime);
	}
}

function render() {
	context.clearRect(0,0,canvas.width, canvas.height);
	drawSnake();
	drawFood();
	drawObstacles();
	drawArea();
	drawScore();
}

function drawScore(){
	context.fillstyle = "red";
	context.font = "30px Arial";
	context.fillText(Score,10,40);
}

function drawFood() {
	drawBlock(Food.x , Food.y , "#FF00FF");
}

function drawObstacles() {
	let i;
	for (i = 1; i < Obstacles.length; i++ ) {
		drawBlock(Obstacles[i].x, Obstacles[i].y, "#00FF00");
	}
}

function drawSnake() {
	drawBlock(Snake.head.x, Snake.head.y, "#FF00FF");
	let i;
	for (i = 1; i < Snake.blocks.length; i++ ) {
		drawBlock(Snake.blocks[i].x, Snake.blocks[i].y, "#FF00FF");
	}
}

function drawArea() {
	let i;
	for(i=0; i < 50; i++) {
		drawBlock(i,0,"#FF0000")
		drawBlock(0,i,"#FF0000")
		drawBlock(i,49,"#FF0000")
		drawBlock(49,i,"#FF0000")
	}
}

function drawBlock(x,y, color) {
	context.beginPath();
	context.fillStyle = "black";
	context.lineWidth="2";
	context.strokeRect(x*10, y*10, 10, 10); 
	context.fillStyle = color;
	context.fillRect(x*10, y*10, 10, 10); 
	context.fill();
	context.closePath();
}


function onKeyDown(e) {
	if ((e.keyCode === KeyEvent.DOM_VK_A || e.keyCode === KeyEvent.DOM_VK_LEFT) && Snake.direction != "LEFT") {
		Snake.direction = "RIGHT"
	}
	else if ((e.keyCode === KeyEvent.DOM_VK_D || e.keyCode === KeyEvent.DOM_VK_RIGHT) && Snake.direction != "RIGHT") {
		Snake.direction = "LEFT";
	}
	else if ((e.keyCode === KeyEvent.DOM_VK_W || e.keyCode === KeyEvent.DOM_VK_UP) && Snake.direction != "DOWN") {
		Snake.direction = "UP";
	}
	else if ((e.keyCode === KeyEvent.DOM_VK_S || e.keyCode === KeyEvent.DOM_VK_DOWN) && Snake.direction != "UP") {
		Snake.direction = "DOWN";
	}
}

window.addEventListener('keydown',onKeyDown);
setup();
requestAnimationFrame(game);
