myGame.sound = (function () {
			
	let sounds = {}

	//------------------------------------------------------------------
	//
	// This function performs the one-time initialization.
	// written by Dean Mathias
	//------------------------------------------------------------------
	function initialize() {
		'use strict';

		function loadSound(source) {
			let sound = new Audio();
			sound.src = source;
			return sound;
		}

		function loadAudio() {
			sounds['audio/sound-1'] = loadSound('scripts/render/assets/audio/Explosion3__001.wav');
			sounds['audio/sound-2'] = loadSound('scripts/render/assets/audio/Pew__004.wav');
			sounds['audio/music'] = loadSound('scripts/render/assets/audio/UndisputedStefanGrossmann.mp3');
		}

		loadAudio();
	}
	
	initialize();

	//------------------------------------------------------------------
	//
	// Plays the specified audio
	// written by Dean Mathias
	//------------------------------------------------------------------
	function playSound(whichSound) {
		sounds[whichSound].play();
	}

	return {
		initialize: initialize,
		playSound: playSound
	};

}());

