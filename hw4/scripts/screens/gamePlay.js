myGame.screens['gamePlay'] = (function(game, systems, objects, renderer, graphics, input, persistence, sound) {
	'use strict';

	let stop_playing = false;
	let currentTime = performance.now();
	let startTime = performance.now();
	let firingDelay = 250;
	let Score = 0;
	let highScores = [];
	let lives = 3;
	
	let myHighScores = persistence;

	let mySounds = sound;

	// keyboard input
	let myKeyboard = input;

	// the ship object	
	let myShip = objects.Ship({
		imageSrc: 'scripts/render/assets/Example_ships/1.png',
		center: { x: graphics.canvas.width/2, y: graphics.canvas.height/2},
		size: { width: 50, height: 50},
		acceleRate: .05,
		boundries: {x:canvas.width, y:canvas.height},
		rotateRate: Math.PI / 500,
		velocity: {x: 0, y:0}
	});
	
	// the first enemy
	let myEnemy2 = objects.Ufo({
		imageSrc: 'scripts/render/assets/Example_ships/10B.png',
		center: { x: Random.nextGaussian(graphics.canvas.width/4, 75), y: Random.nextGaussian(graphics.canvas.height/2, 75)},
		size: { width: 50, height: 50},
		acceleRate: .5,
		boundries: {x:canvas.width, y:canvas.height},
		rotateRate: Math.PI / 500,
		velocity: {x: 0, y:0}
	});

	//the second enemy
	let myEnemy1 = objects.Ufo({
		imageSrc: 'scripts/render/assets/Example_ships/13B.png',
		center: { x: 3*(Random.nextGaussian(graphics.canvas.width / 4, 75)), y: Random.nextGaussian(graphics.canvas.height / 2, 75)},
		size: { width: 50, height: 50},
		acceleRate: .5,
		boundries: {x:canvas.width, y:canvas.height},
		rotateRate: Math.PI / 500,
		velocity: {x: 0, y:0}
	});
	
    let particles = systems.ParticleSystem({
        center: { x: myShip.center.x, y: myShip.center.y},
        size: { mean: 6, stdev: 3 },
        speed: { mean: 45, stdev: 15 },
        lifetime: { mean: 2, stdev: 1}
    });
    let rocketRenderer= renderer.ParticleSystem(particles, graphics, 
        'scripts/render/assets/particles/yellowlight.png');

	// list of projectiles;	
	let Beams = [];

	// list of asteroids	
	let Asteroids = [];
	
	function processInput(elapsedTime) {
		myKeyboard.update(elapsedTime);
	}

	function update(elapsedTime) {
		let newAsteroids = [];
		let gameEnded = false; 

		myShip.update(elapsedTime);
		myEnemy1.update(elapsedTime);
		myEnemy2.update(elapsedTime);
		
		for (let i =0; i < Beams.length; i++){
			let collided = false;
			//Asteroids
			for (let j = 0; j < Asteroids.length; j++ ){
				let asteroid = Asteroids[j]
				if(collide(Beams[i], asteroid)) {
					
					if(asteroid.level == 2){
						for(let k = 0; k < 4; k++) {
							let myAsteroid = objects.Asteroid({
								imageSrc: 'scripts/render/assets/large.png',
								size: { width: 35, height: 35},
								boundries: { x:canvas.width, y:canvas.height},
								center : {x: asteroid.center.x, y: asteroid.center.y},
								level: 1
							});
							newAsteroids.push(myAsteroid);
						}
					}else if(asteroid.level == 1){
						for(let k = 0; k < 3; k++) {
							let myAsteroid = objects.Asteroid({
								imageSrc: 'scripts/render/assets/small.png',
								size: { width: 20, height: 20},
								boundries: { x:canvas.width, y:canvas.height},
								center : {x: asteroid.center.x, y: asteroid.center.y},
								level: 0
							});
							newAsteroids.push(myAsteroid);
						}
					}else {}
					Asteroids[j].destroyed = true;
					collided = true;
				}
			}
			
			// Borders
			if(Beams[i].center.x >  canvas.width || Beams[i].center.x < 0) {
				collided = true;
			}
			if(Beams[i].center.y >  canvas.height || Beams[i].center.y < 0) {
				collided = true;
			}
			// Enemies
			if(collide(Beams[i], myEnemy1)) {
				collided = true;
				particles.explosion(Beams[i].center, Beams[i].rotation);
				mySounds.playSound('audio/sound-1');
			}
			if(collide(Beams[i], myEnemy2)) {
				collided = true;
				particles.explosion(Beams[i].center, Beams[i].rotation);
				mySounds.playSound('audio/sound-1');
			}

			if(!collided) {
				Beams[i].update(elapsedTime);
			}else {
				particles.explosion(Beams[i].center, Beams[i].rotation);
				mySounds.playSound('audio/sound-1');
				Beams.splice(i,1);
			}
			
		}

		for (let i = 0; i < Asteroids.length; i++){
			
			if(collide(Asteroids[i], myShip)) {
				particles.explosion(Asteroids[i].center, Asteroids[i].rotation);
				mySounds.playSound('audio/sound-1');
				Asteroids[i].destroyed = true;
				if(lives > 1) {
					myShip.explode();
					lives--;
				} else {
					gameEnded = true;
				}
			}
			
			if(Asteroids[i].destroyed) {
				Asteroids.splice(i,1);
			} else {
				Asteroids[i].update(elapsedTime);
			}

		}

		firingDelay -= elapsedTime;

		particles.update(elapsedTime, myShip.center, myShip.rotation);
		for (let i = 0; i < newAsteroids.length; i++) {
			Asteroids.push(newAsteroids[i]);
		}

		if(gameEnded) {
			gameOver();
		}
	}
	
	// x1 = {
	// 		center: {x:, y:}
	// 		}
	// x2 = {
	// 		center: {x:, y:}
	// 		}
	
	function distance(p1, p2) {
		let x_1 = p1.center.x
		let x_2 = p2.center.x
		let y_1 = p1.center.y
		let y_2 = p2.center.y

		return Math.sqrt((x_2 - x_1)*(x_2 - x_1) + (y_2 - y_1)*(y_2 - y_1));
	}
	// x = {
	// 		size : {height: , width: }
	// 		}

	function radius(x){
		if(x.size.height > x.size.width) {
			return x.size.height / 2;
		}
		else {
			return x.size.width / 2;
		}
	}

	function collide(obj1, obj2){
		if(distance(obj1 ,obj2) < radius(obj1) + radius(obj2)){
			return true;
		}
		else {
			return false;
		}
	}

	function render() {
		graphics.clear();
		
		graphics.drawText({
			font: '30px Arial',
			fillStyle: '#F8F869',
			strokeStyle: '#F8F869',
			position: {x: 50, y:50},
			rotation: 0,
			text:"Score: "+ Score + " | Time: " + Math.round((performance.now() - startTime) / 1000) + " | Lives: " + lives
		});
		
		rocketRenderer.render();
		
		renderer.Ship.render(myShip);
	
		renderer.Ufo.render(myEnemy1);
		renderer.Ufo.render(myEnemy2);

		for (let i =0; i < Beams.length; i++){
			renderer.Beam.render(Beams[i]);
		}
		for (let i =0; i < Asteroids.length; i++){
			renderer.Asteroid.render(Asteroids[i]);
		}

	}

	function gameLoop(time) {
		let elapsedTime = time - currentTime;
		currentTime = time;
		processInput(elapsedTime);
		update(elapsedTime);
		render();

		if (!stop_playing) {
			requestAnimationFrame(gameLoop);
		}
	}

	function gameOver() {
		myHighScores.add(Math.round((performance.now() - startTime) / 1000), Score);
		stop_playing = true;
		game.displayScreen('landing');
		Asteroids = [];
		Beams = [];
		lives = 3;
		Score = 0;
	}
	
	function init() {
		
		stop_playing = false;
		mySounds.playSound('audio/music');

		myKeyboard.register('w', function() {
			particles.addParticles();
			myShip.moveForward();
		});
		myKeyboard.register('d', myShip.rotateRight);
		myKeyboard.register('a', myShip.rotateLeft);
	
		myKeyboard.register('ArrowUp', function() {
			particles.addParticles();
			myShip.moveForward();
		});
		myKeyboard.register('ArrowRight', myShip.rotateRight);
		myKeyboard.register('ArrowLeft', myShip.rotateLeft);
		
		myKeyboard.register('t', function () {
			
		});
	
		myKeyboard.register(' ', function() {
			if(firingDelay <= 0){
				mySounds.playSound('audio/sound-2');
				let myBeam = objects.Beam(
					myShip.fire()
				);
				Beams.push(myBeam);
				firingDelay = 250;
				let myAsteroid = objects.Asteroid({
					imageSrc: 'scripts/render/assets/Asteroid2.png',
					size: { width: 50, height: 50},
					center: {x:500, y:500},
					boundries: { x:canvas.width, y:canvas.height},
					level: 2
				});
				Asteroids.push(myAsteroid);
				Score +=1;
			}
		});

		myKeyboard.register('Escape', function() {
			stop_playing = true;
			game.displayScreen('landing');
		});

		startTime = performance.now();

	}

	function run() {
		stop_playing = false;
		startTime = performance.now();
		currentTime = performance.now();
		requestAnimationFrame(gameLoop);
	}

	return {
		init : init,
		run : run
	};

}(myGame.game, myGame.systems, myGame.objects, myGame.render, myGame.graphics, myGame.input, myGame.persistence, myGame.sound));
