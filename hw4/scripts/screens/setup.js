myGame.screens['setup'] = (function(game) {
	'use strict';

	function init() {
		document.getElementById("setup-back").addEventListener('click',
			function() { 
				game.displayScreen('landing'); 
			});
		document.getElementById("setup-gamePlay").addEventListener('click',
			function() {
				game.displayScreen('gamePlay'); 
			});
	}

	function run() {}

	return {
		init : init,
		run : run,
	};
}(myGame.game));
