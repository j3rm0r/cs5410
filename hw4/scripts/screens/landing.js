myGame.screens["landing"] = (function(game) {
	'use strict';

	function init() {
		
		document.getElementById("landing-highscores").addEventListener('click',
			function() {
				game.displayScreen('highscores'); 
			});
		document.getElementById('landing-newGame').addEventListener('click',
			function() {
				game.displayScreen("setup"); 
			});
		document.getElementById('landing-about').addEventListener('click',
			function() {
				game.displayScreen("about"); 
			});
	}


	function run() {
		// nothing to do
	}

	return {
		init : init,
		run : run
	};
}(myGame.game));
