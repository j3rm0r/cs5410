myGame.input = (function() {

	let keyboard = { 
		keys: {},
		handlers: {}
	};
	let available = true;
	
	function keyDown(e) {
		if(available) {
			keyboard.keys[e.key] = e.timeStamp;
			available = false;
		}
	}

	function keyUp(e) {
		delete keyboard.keys[e.key];
		available = true;
	}
	
	keyboard.update = function (time) {
		for (key in keyboard.keys) {
			if(keyboard.keys.hasOwnProperty(key)) {
				if (keyboard.handlers[key]) {
					keyboard.handlers[key](time)
				}
			}
		}
	};

	keyboard.register = function (key, handler) {
		keyboard.handlers[key] = handler;
	}

	keyboard.unregister = function (key, handler) {
		delete keyboard.handlers[key];
	}

	window.addEventListener('keydown', keyDown);
	window.addEventListener('keyup', keyUp);
	
	return keyboard;

}());
