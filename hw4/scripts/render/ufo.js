// --------------------------------------------------------------
//
// Renders a Ufo object.
// Identical to the demonstration Dean provided for the Logo object
//
// spec = {
//    image: ,
//    center: { x: , y: },
//    size: { width: , height: }
// }
//
// --------------------------------------------------------------
myGame.render.Ufo = (function(graphics) {
    'use strict';

    function render(spec) {
        if (spec.imageReady) {
            graphics.drawTexture(spec.image, spec.center, spec.rotation, spec.size);
        }
    }

    return {
        render: render
    };
}(myGame.graphics));
