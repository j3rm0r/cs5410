// --------------------------------------------------------------
//
// Creates a Ship object, with functions for managing state.
// identical to the logo object Dean provided in the game state demo
//
// spec = {
//    imageSrc: ,   // Web server location of the image of the ship
//    center: { x: , y: },
//    size: { width: , height: }
//    boundries: {x:, y:},
//    rotateRate:,
//    acceleRate:,
// }
//
// --------------------------------------------------------------
myGame.objects.Ship = function(spec) {
    'use strict';

    let rotation = 0;
    let direction = 0;
    let imageReady = false;
    let image = new Image();
	let velocity = {x: 0, y:0}


    image.onload = function() {
        imageReady = true;
    };
    image.src = spec.imageSrc;

    function rotateLeft(elapsedTime) {
        rotation -= (spec.rotateRate * elapsedTime);
    }
    function rotateRight(elapsedTime) {
        rotation += (spec.rotateRate * elapsedTime);
    }

	function fire() {
		return {
			rotation: rotation,
			imageSrc: 'scripts/render/assets/beam.png',
			center: {x: spec.center.x, y:spec.center.y},
			size: {width: 25, height:25},
			velocity: {x: 10*Math.cos(rotation), y: 10*Math.sin(rotation)}
		}
	}

    function moveForward(elapsedTime) {
		direction = rotation;
		velocity.x += spec.acceleRate * Math.cos(rotation);
		velocity.y += spec.acceleRate * Math.sin(rotation);
    }

	function update(elapsedTime) {
		if(spec.center.x + velocity.x < spec.size.width) {
			spec.center.x = spec.boundries.x - spec.size.width;
		}	
		if(spec.center.x + velocity.x > spec.boundries.x - spec.size.width){
			spec.center.x = spec.size.width;
		}
		if(spec.center.y + velocity.y > spec.boundries.y - spec.size.height ) {
			spec.center.y = spec.size.height;
		}
		if(spec.center.y + velocity.y < spec.size.height) {
			spec.center.y = spec.boundries.y - spec.size.height
		}
		spec.center.x += velocity.x;
		spec.center.y += velocity.y
	}
	
	function explode() {
		spec.center.x = spec.boundries.x / 2;
		spec.center.y = spec.boundries.y / 2;
	}

    let api = {
		rotateLeft: rotateLeft,
		rotateRight: rotateRight,
		moveForward: moveForward,
		fire: fire,
		update: update,
		explode: explode,
        get imageReady() { return imageReady; },
        get rotation() { return rotation; },
        get image() { return image; },
        get center() { return spec.center; },
        get size() { return spec.size; }
    };

    return api;
}
