// --------------------------------------------------------------
//
// Creates a Beam object, with functions for managing state.
// identical to the logo object Dean provided in the game state demo
//
// spec = {
//		rotation:
//    imageSrc: ,   // Web server location of the image
//    center: { x: , y: },
//    size: { width: , height: },
//    velocity: {x:, y:},
//    rotation: 
// }
//
// --------------------------------------------------------------
myGame.objects.Beam = function(spec) {
    'use strict';

    let imageReady = false;
    let image = new Image();

    image.onload = function() {
        imageReady = true;
    };
    
	image.src = spec.imageSrc;

	function update(elapsedTime) {
		spec.center.x += spec.velocity.x;
		spec.center.y += spec.velocity.y;
	}

    let api = {
		update: update,
		get rotation() { return spec.rotation; },
        get imageReady() { return imageReady; },
        get image() { return image; },
        get center() { return spec.center; },
        get size() { return spec.size; }
    };

    return api;
}
