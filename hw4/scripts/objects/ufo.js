// --------------------------------------------------------------
//
// Creates a Ufo object, with functions for managing state.
// identical to the logo object Dean provided in the game state demo
//
// spec = {
//    imageSrc: ,   // Web server location of the image of the ship
//    center: { x: , y: },
//    size: { width: , height: }
//    boundries: {x:, y:},
// }
//
// --------------------------------------------------------------
myGame.objects.Ufo= function(spec) {
    'use strict';

    let rotation = 0;
	let direction = 0;
    let imageReady = false;
    let image = new Image();
	let velocity = {x: Random.nextGaussian(0,1), y: Random.nextGaussian(0,1)}

    image.onload = function() {
        imageReady = true;
    };
    image.src = spec.imageSrc;

	function update(elapsedTime) {
		if(spec.center.x + velocity.x < spec.size.width) {
			spec.center.x = spec.boundries.x - spec.size.width;
		}	
		if(spec.center.x + velocity.x > spec.boundries.x - spec.size.width){
			spec.center.x = spec.size.width;
		}
		if(spec.center.y + velocity.y > spec.boundries.y - spec.size.height ) {
			spec.center.y = spec.size.height;
		}
		if(spec.center.y + velocity.y < spec.size.height) {
			spec.center.y = spec.boundries.y - spec.size.height
		}
		spec.center.x += velocity.x;
		spec.center.y += velocity.y
	}
	
	function fire() {
		return {
			rotation: rotation,
			imageSrc: 'scripts/render/assets/beam.png',
			center: {x: spec.center.x, y:spec.center.y},
			size: {width: 25, height:25},
			velocity: {x: 10*Math.cos(rotation), y: 10*Math.sin(rotation)}
		}
	}


    let api = {
		update: update,
        get imageReady() { return imageReady; },
        get rotation() { return rotation; },
        get image() { return image; },
        get center() { return spec.center; },
        get size() { return spec.size; }
    };

    return api;
}
