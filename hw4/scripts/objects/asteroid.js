// --------------------------------------------------------------
//
// Creates a Asteroid object, with functions for managing state.
// identical to the logo object Dean provided in the game state demo
//
// spec = {
//    imageSrc: ,   // Web server location of the image of the ship
//    size: { width: , height: }
//    boundries: {x:, y:}
//    spec.center: {x: ,y:}
//
// }
//
// --------------------------------------------------------------
myGame.objects.Asteroid= function(spec) {
    'use strict';

    let rotation = Random.nextGaussian(Math.PI, 2);
	let rotateRate = Math.PI/1000;
    let imageReady = false;
    let image = new Image();
	let velocity = { x:Random.nextGaussian(0,3), y:Random.nextGaussian(0,3)}
	let destroyed = false;

    image.onload = function() {
        imageReady = true;
    };
    image.src = spec.imageSrc;

	function update(elapsedTime) {
		if(spec.center.x + velocity.x < spec.size.width) {
			spec.center.x = spec.boundries.x - spec.size.width;
		}	
		if(spec.center.x + velocity.x > spec.boundries.x - spec.size.width){
			spec.center.x = spec.size.width;
		}
		if(spec.center.y + velocity.y > spec.boundries.y - spec.size.height ) {
			spec.center.y = spec.size.height;
		}
		if(spec.center.y + velocity.y < spec.size.height) {
			spec.center.y = spec.boundries.y - spec.size.height
		}
		rotation += (rotateRate*elapsedTime);
		spec.center.x += velocity.x;
		spec.center.y += velocity.y
	}

    let api = {
		update: update,
        get imageReady() { return imageReady; },
        get rotation() { return rotation; },
        get image() { return image; },
        get center() { return spec.center; },
        get size() { return spec.size; },
        get level() { return spec.level; }
    };

    return api;
}
