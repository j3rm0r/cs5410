
// ------------------------------------------------------------------
myGame.persistence = (function () {
	'use strict';
	let highScores = {};
	let previousScores = localStorage.getItem('myGame.highScores');

	if (previousScores !== null) {
		highScores = JSON.parse(previousScores);
	}

	function add(key, value) {
		highScores[key] = value;
		localStorage['myGame.highScores'] = JSON.stringify(highScores);
	}

	function remove(key) {
		delete highScores[key];
		localStorage['myGame.highScores'] = JSON.stringify(highScores);
	}

	function report() {
		let htmlNode = document.getElementById('highscores-list');
		
		htmlNode.innerHTML = '';
		for (let key in highScores) {
			let score = document.createTextNode('Time: ' + key + ' Score: ' + highScores[key])
			let li_score = document.createElement('li');
			li_score.appendChild(score);
			htmlNode.appendChild(li_score);
		}
		htmlNode.scrollTop = htmlNode.scrollHeight;
	}

	return {
		add : add,
		remove : remove,
		report : report
	};
}());

