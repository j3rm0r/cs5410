// Renders a Character object
// spec = {
// 		image
// 		center {x:, y:}
// 		size
// 		}

myGame.render.Character = (function(graphics) {
	'use strict'

	function render(spec)  {
		if(spec.imageReady) {
			graphics.drawTexture(spec.image, spec.center, spec.size);
		}
	}
	return {
		render: render
	};
}(myGame.graphics));
