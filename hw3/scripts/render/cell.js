// Renders a Cell object
// spec = {
// 		center: {row:, column:}
// 		size:
// 		}


myGame.render.Cell= (function(graphics) {
	'use strict'

	function render(spec) {
		graphics.drawLine(
			{x: spec.center.row , y: spec.center.column},
			{x: spec.center.row+1, y: spec.center.column},
			spec.size
		);
		graphics.drawLine(
			{x: spec.center.row, y: spec.center.column},
			{x: spec.center.row, y: spec.center.column+1},
			spec.size
		);
		graphics.stroke();
	}

	return {

		render: render
	};
}(myGame.graphics));
