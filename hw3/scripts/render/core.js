myGame.graphics = (function() {
	'use strict' 
	
	let canvas = document.getElementById('canvas');
	let context = canvas.getContext('2d');

	let background = new Image();
	background.isReady = false;
	background.onload = function() {
		this.isReady = true;
	};
	background.src = 'hyptosis_til-art-batch-2_grass.png';
	
//	let nest = new Image();
//	nest.isReady = false;
//	nest.onload = function() {
//		this.isReady = true;
//	};
//	background.src = '/home/jer/Workspace/cs5410/hw3/scripts/render/nest.png';

	function drawNest(size) {
		if(nest.isReady) {
			context.drawImage(nest, 
				(size-1) * (canvas.width / size), (size -1) * (canvas.height / size),
				canvas.width / (size) , canvas.height / (size));
		}	
	}

	function clear() {
		context.clearRect(0, 0, canvas.width ,canvas.height);
	}

	//expects a character object with
	// center: {x:, y:}
	// image
	// size
	function drawTexture(image, center, size) {
		context.drawImage(image, 
			center.x * (canvas.width / size), center.y * (canvas.height / size),
			canvas.width / (size) , canvas.height / (size));
	}
	function drawText(message, y, x) {
		context.font = '30px Arial';
		context.fillStyle = 'red';
		context.fillText(message, y, x);
	}

	// expects a cell object with the following
	// x:,
	// y:,
	// edges: {
	// 		n:,
	// 		s:,
	// 		e:,
	// 		w:,
	// 		}
	// 	size:
	function drawCell(cell, size) {
		if(background.isReady) {
			context.drawImage(background, 
				cell.x * (canvas.width / size), cell.y * (canvas.height / size),
				canvas.width / size , canvas.height / size);
		}

		context.strokeStyle = 'rgb(94, 112, 26)';
		context.lineWidth = 10;
		context.beginPath();

		if(cell.edges.n === null) {
			context.moveTo(cell.x * (canvas.width / size), cell.y * (canvas.height / size));
			context.lineTo((cell.x + 1) * (canvas.width / size), cell.y * (canvas.height / size)) 
		}
		if(cell.edges.s === null) {
			context.moveTo(cell.x * (canvas.width / size), (cell.y +1) * (canvas.height / size));
			context.lineTo((cell.x + 1) * (canvas.width / size), (cell.y +1) * (canvas.height / size));
		}
		if(cell.edges.e === null) {
			context.moveTo((cell.x +1) * (canvas.width / size), cell.y * (canvas.height / size));
			context.lineTo((cell.x + 1) * (canvas.width / size), (cell.y +1) * (canvas.height /size)); 
		}
		if(cell.edges.w === null) {
			context.moveTo(cell.x * (canvas.width / size), cell.y * (canvas.height / size));
			context.lineTo(cell.x * (canvas.width / size), (cell.y +1) * (canvas.height / size));
		}

		context.stroke();
		
	}

	function drawBorder(){
		context.beginPath();
		context.moveTo(0, 0);
		context.lineTo(canvas.width -1, 0);
		context.lineTo(canvas.width -1,canvas.height -1);
		context.lineTo(0, canvas.height -1);
		context.closePath();
		context.strokeStyle = 'rgb(0, 0, 0)';
		context.stroke();
	}
	
	function stroke() {
		context.stroke()
	}
	

	let api = {
		get canvas() {return canvas},
		clear: clear,
		drawBorder: drawBorder,
		drawCell: drawCell,
		drawTexture: drawTexture,
		stroke: stroke,
		drawText: drawText,
		drawNest: drawNest
	}

	return api;
}());
