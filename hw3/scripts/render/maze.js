// Renders a Maze object
// spec = {
// 		image
// 		maze [][]
// 		}

myGame.render.Maze = (function(graphics) {
	'use strict'

	function render(spec) {
		for( let row = 0; row < spec.maze.length; row++) {
			for( let col = 0; col< spec.maze.length; col++){
				graphics.drawCell(spec.maze[row][col], spec.maze.length);
			}
		}
		graphics.drawBorder();
	}
	return {
		render: render
	};
}(myGame.graphics));
