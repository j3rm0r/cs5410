myGame.screens['gamePlay'] = (function(game, objects, renderer, graphics, input) {
	'use strict';

	let stop_playing = false;
	let currentTime = performance.now();
	let startTime = performance.now();
	let side = 5; 
	let Delay = 100;
	let Score = 0;
	let highScores = [];

	let spec = {size: side}
	let myMaze = objects.Maze(spec);
	
	let myKeyboard = input;
	
	let character = {
		center: {x: 0, y:0},
		size: side,
		src: 'chicken.png',
		edges : myMaze.maze[0][0].edges 
	}
	
	let myCharacter = objects.Character(character);

	function processInput(time) {
		if(Delay > 0) {
			Delay = Delay - time;
		} else {
			myKeyboard.update(time);
			Delay =90;
		}
	}

	function update() {
		myCharacter.edge = myMaze.maze[myCharacter.center.y][myCharacter.center.x].edges;
		if(myCharacter.center.x == side-1 && myCharacter.center.y == side -1 ) {
			console.log(myCharacter.center);
			gameOver();
		}
	}

	function render() {
		renderer.Maze.render(myMaze);
		renderer.Character.render(myCharacter);
		//graphics.drawNest(side);
		graphics.drawText( '' + Score, 50, 50);
		graphics.drawText( '' + Math.round((performance.now() - startTime)/1000), 50, 80);

	}

	function gameLoop(time) {
		let elapsedTime = time - currentTime;
		currentTime = time;
		processInput(elapsedTime);
		update(elapsedTime);
		render();

		if (!stop_playing) {
			requestAnimationFrame(gameLoop);
		}
	}

	function gameOver() {
		highScores.push(Score);
		highScores.sort(function(a,b) {return b-a});
		
		let DOMHighscoreDiv = document.getElementById("highscores-list")
		while(DOMHighscoreDiv.hasChildNodes()){
			DOMHighscoreDiv.removeChild(DOMHighscoreDiv.lastChild);
		}
		let DOMHighscoreList = document.createElement("ol");
		DOMHighscoreDiv.appendChild(DOMHighscoreList);
		let i;
		for (i=0;i<10 && i < highScores.length;i++) {
			let newEntry = document.createElement("li");
			newEntry.innerText="" + highScores[i];
			DOMHighscoreList.appendChild(newEntry);
		}
		init();
		stop_playing = true;
		game.displayScreen('landing');
	}
	
	function init() {
		
		stop_playing = false;
		side = myGame.screens.setup.getSize(); 

		myKeyboard.register('d', myCharacter.moveRight);
		myKeyboard.register('a', myCharacter.moveLeft);
		myKeyboard.register('s', myCharacter.moveDown);
		myKeyboard.register('w', myCharacter.moveUp);
		
		myKeyboard.register('Escape', function() {
			stop_playing = true;
			game.displayScreen('landing');
		});
		startTime = performance.now();

	}

	function run() {
		stop_playing = false;
		side = myGame.screens.setup.getSize(); 
		spec = {size: side}
		myMaze = objects.Maze(spec);
		character.size = side;
		myCharacter = objects.Character(character);
		currentTime = performance.now();
		requestAnimationFrame(gameLoop);
	}

	return {
		init : init,
		run : run
	};

}(myGame.game, myGame.objects, myGame.render, myGame.graphics, myGame.input));
