myGame.screens["highscores"] = (function(game, objects, render, graphics, input) {
	
	'use strict';
	let highScores = [];

	function init() {
		document.getElementById('highscores-back').addEventListener('click',
			function() {
				game.displayScreen("landing"); 
			});
	}

	function updateList(newScore) {
		highscores.push(newScore);
		// doing a numerical sort need to provide a comparator
		// https://www.w3schools.com/js/js_array_sort.asp
		highscores.sort(function (a,b) {return a-b});
	}

	function run() {
		let list = document.getElementById('highscores-list');
		while (list.hasChildNodes()) {
			list.removeChild(list.firstChild)
		}
		for( let i = 0; i < 10 && i < highScores.length; i++) {
			let score = document.createTextNode(highScores[i]);
			let li_score = document.createElement('li');
			li_score.appendChild(score);
			list.appendChild(li_score);
		}
	}

	return {
		init : init,
		run : run,
		updateList : updateList
	};
}(myGame.game));
