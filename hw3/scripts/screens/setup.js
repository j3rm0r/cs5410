myGame.screens['setup'] = (function(game) {
	'use strict';

	function init() {
		document.getElementById("setup-back").addEventListener('click',
			function() {
				game.displayScreen('landing'); 
			});
		document.getElementById("setup-gamePlay").addEventListener('click',
			function() {
				game.displayScreen('gamePlay'); 
			});
		document.getElementById('5').checked = true;
	}

	function getSize() {
		let sizes = document.getElementsByClassName('radio-button')
		for (let i =0; i < sizes.length; i++) {
			if(sizes[i].checked) {
				return Number(sizes[i].id)
			}
		}
		
	}

	function run() {}

	return {
		init : init,
		run : run,
		getSize : getSize
	};
}(myGame.game));
