myGame.screens['about'] = (function(game) {
	'use strict';

	function init() {
		document.getElementById("about-back").addEventListener('click',
			function() {
				game.displayScreen('landing'); 
			});
	}
	function run() {}

	return {
		init : init,
		run : run
	};
}(myGame.game));
