// spec should have 
// 	src 		 path to the image you want to use
// 	center {x:, y:}
// 	size
//	edges : {
//		n: null,
//		s: null,
//		e: null,
//		w: null
//	}
//
myGame.objects.Character = function(spec) {

	let imageReady =  false;
	let image = new Image();

	image.onload = function () {
		imageReady = true;
	};
	image.src = spec.src;

	function moveLeft() {
		// if there is a node to the east replace spec center with that
		if (spec.edges.w != null) {
			spec.center={x: spec.edges.w.x, y: spec.edges.w.y};
		}
	}

	function moveRight() {
		// if there is a node to the west replace spec center with that
		if(spec.edges.e != null) {
			spec.center={x: spec.edges.e.x, y: spec.edges.e.y};
		}
		
	}

	function moveUp() {
		// if there is a node to the north replace spec center with that
		if(spec.edges.n != null) {
			spec.center={x: spec.edges.n.x, y: spec.edges.n.y};
		}
	}

	function moveDown() {
		// if there is a node to the south replace spec center with that
		if(spec.edges.s != null) {
			spec.center = {x: spec.edges.s.x, y: spec.edges.s.y};
		}
	}


	
	let api = {
		moveUp: moveUp,
		moveDown: moveDown,
		moveRight: moveRight,
		moveLeft: moveLeft,
		set edge(list) {spec.edges = list; },
		get imageReady() {return imageReady; },
		get image() {return image; },
		get center() {return spec.center; },
		get size() {return spec.size; },
		get edges() {return spec.edges;}
	}

	return api;
};
