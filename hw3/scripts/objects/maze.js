// spec should have 
// 	side: 			a side length that gets turned in to the size (5x5, 10x10, 15x15, or 20x20)	

myGame.objects.Maze = function(spec) {
	
	let frontier, processed ,graph = [];

	let maze = prims(); 

	
	function frontNeighbors(row, column) {
		let neighbors = getNeighbors(row, column);
		let front = [];
		for(let i = 0; i < neighbors.length; i++) {
			let x = neighbors[i].x;
			let y = neighbors[i].y;
			if(frontier.indexOf(graph[y][x]) == -1 && processed.indexOf(graph[y][x]) == -1) {
				front.push(graph[y][x]);
			}
		}
		return front;
	}

	function getNeighborsInMaze(row, column) {
		let neighborsInMaze = getNeighbors(row, column);
		let neighbors = []
		for(let i = 0; i < neighborsInMaze.length; i++) {
			let x = neighborsInMaze[i].x;
			let y = neighborsInMaze[i].y;
			if(processed.indexOf(graph[y][x]) != -1 ) {
				neighbors.push(graph[y][x]);
			}
		}
		return neighbors;
	}

	function getNeighbors(row, column) {
		let neighbors = [];
		if(row +1 < spec.size ){
			neighbors.push(graph[row +1][column]);
		}
		if(row -1 >= 0 ){
				neighbors.push(graph[row -1][column]);
		}
		if(column +1 < spec.size ){
			neighbors.push(graph[row][column +1]);
		}
		if(column -1 >= 0 ){
				neighbors.push(graph[row][column -1]);
		}
		return neighbors
	}

	function prims() {
		
		graph = [];
		for (let row = 0; row < spec.size; row++) {
			graph.push([]);
			for (let col = 0; col < spec.size; col++) {
				graph[row].push(
					{x: col, y: row, edges : {
						n: null,
						s: null,
						e: null,
						w: null,
					}}
				);
			}
		}
		//randomly select a cell in the graph, add it to the maze and make all it's neighbors a member of the frontier
		let row = Math.floor(Math.random() * spec.size);
		let column = Math.floor(Math.random() * spec.size);
		processed = [];	
		frontier = [];
		processed.push(graph[row][column]);
		// make frontier grow before
		frontier = frontier.concat(frontNeighbors(row, column));

		while (frontier.length) {
		//randomly choose a cell in the frontier, choose one of it's neighbor cells that is in the maize, remove the wall between them and add that cell to the maize
			let index = Math.floor(Math.random() * frontier.length);
			chosenFrontierCell = frontier[index];
			
			x = chosenFrontierCell.x;
			y = chosenFrontierCell.y;
			
			
			// get a cell that is in the maize but borders the frontier cell
			let neighbors = getNeighborsInMaze(y,x);
			let index2 = Math.floor(Math.random() * neighbors.length);
			let neighbor = neighbors[index2];

			if(neighbors.length == 0) {	
				frontier.splice(index,1);
				continue;
			} else {
				let col = neighbor.x;
				let row = neighbor.y;
				
				if(x === col) {
					if(y > row) {
						graph[row][x].edges.s = graph[y][x];
						graph[y][x].edges.n = graph[row][x];
					}else if (y < row) {
						graph[row][x].edges.n = graph[y][x];
						graph[y][x].edges.s = graph[row][x];
					
					}else {
						console.log("x === row and y == col");	
					}
					
				} else if (y === row) {
					if(x > col) {
						graph[y][col].edges.e = graph[y][x];
						graph[y][x].edges.w = graph[y][col];
							
					}else if (x < col) {
						graph[y][col].edges.w = graph[y][x];
						graph[y][x].edges.e = graph[y][col];

					}else {
						console.log("y === col and x === row");
					}
				}
				
				processed.push(graph[y][x]);
				frontier = frontier.concat(frontNeighbors(y, x));
				frontier.splice(index,1);
				
			}	
		}
		//maze[row][column]
		
		return graph;
	}

	

	let api = {
		get maze() { return maze }
	}

	return api;
};
